#include "File.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool ReadFile(const char* Directory, File* InFile)
{
	long Length = 0;
	FILE* F = fopen(Directory, "rb");
	if(F)
	{
		fseek(F, 0, SEEK_END);
		Length = ftell(F);
		fseek(F, 0, SEEK_SET);
		InFile->Content = (char*)malloc((Length+1)*sizeof(char));
		if(InFile->Content)
		{
			fread(InFile->Content, sizeof(char), Length, F);
		}
		fclose(F);
		InFile->Name = (char*)Directory;
		InFile->Content[Length] = 0;
		return true;
	}
	return false;
}

File* NewFile()
{
	File* Ret = malloc(sizeof(File));
	Ret->TrueLocation = 0;
	Ret->CurrentLine = 0;
	Ret->CurrentLocation = 0;
	Ret->Buffer = "";
	return Ret;
}

void Delete(File* InFile)
{
	free(InFile->Content);
	free(InFile);
}

#define ISNEWLINE(c) c == '\n' || c == '\r'
#define ISWHITESPACE(c) c == ' ' || c == '\r' || c == '\t' || c == '\n'

char Next(File* InFile)
{
	if(strlen(InFile->Content) < (InFile->TrueLocation+1))
		return 0;

	char Ret = InFile->Content[++InFile->TrueLocation];
	if(ISNEWLINE(Ret))
	{
		InFile->CurrentLine++;
		InFile->CurrentLocation = 0;
	}
	else
	{
		InFile->CurrentLocation++;
	}
	return Ret;
}

char Peek(File* InFile)
{
	//if(strlen(InFile->Content) > (InFile->TrueLocation+1))
	//	return 0;
	return (char)InFile->Content[InFile->TrueLocation+1];
}

void Push(File* InFile, char ToPush)
{
	strcat(InFile->Content, &ToPush);
}

char Pop(File* InFile)
{
	return (InFile->Content[strlen(InFile->Content)-1]) = 0;
}

void Clear(File* InFile)
{
	memset(InFile->Buffer, 0, strlen(InFile->Buffer)*sizeof(char));
}