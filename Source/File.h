#ifndef FILE_H
#define FILE_H

#include <stdbool.h>

typedef struct {
	char* Name;
	char* Content;
	int CurrentLine;
	int CurrentLocation;
	int TrueLocation;
	char* Buffer;
} File;

bool ReadFile(const char* Directory, File* InFile);
File* NewFile();
void Delete(File* InFile);

char Next(File* InFile);
char Peek(File* InFile);

void Push(File* InFile, char ToPush);
char Pop(File* InFile);
void Clear(File* InFile);

#endif //FILE_H
