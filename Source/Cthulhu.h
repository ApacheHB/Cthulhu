#ifndef CTHULHU_H
#define CTHULHU_H

#include <stdbool.h>

extern bool Verbose;
extern char* SourceFiles[];
extern int NumberOfSourceFiles;
extern char* OutputName;

int Max(int A, int B);

#endif //CTHULHU_H