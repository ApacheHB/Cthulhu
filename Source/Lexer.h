#ifndef LEXER_H
#define LEXER_H

#include "File.h"

enum {
	Keyword,
	Identifier,
	String,
	Number,
	Floating,
	Error,
	EOF,
};

#define KEYWORD(E, S) E,

typedef enum {
	#include "Keywords.inc"
	EX_EOF,
} Keywords;

#undef KEYWORD

typedef struct {
	char* FileName; //name of file lex is in
	int Line; //line of token
	int Location; //location of token

	int Kind; //kind of lexeme
	int Count; //how far into the file is this lexeme
	union {
		int ID; //either the ID of the keyword or the value of an int literal
		char* Content; //either the name of an identifier or the value of a string literal
		float Value; //the value of a float literal
	};
} Lexeme;

Lexeme* GetNextLexeme(File* InFile);
void DeleteLexeme(Lexeme* InLex);

#endif //LEXER_H
