#include "Cthulhu.h"
#include "File.h"
#include "Lexer.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <time.h>

#define MAX_SOURCEFILES 20000

bool Verbose = false;

char* SourceFiles[MAX_SOURCEFILES];
int NumberOfSourceFiles = 0;

char* OutputName;
static bool NameSet = false;

static void Append(char* Array[], char* ToAppend)
{
	SourceFiles[NumberOfSourceFiles++] = ToAppend;
	if(NumberOfSourceFiles > MAX_SOURCEFILES)
	{
		printf("Too many sourcefiles have been registered, max is %d\n", MAX_SOURCEFILES);
		exit(1);
	}
}

static void PrintHelp()
{
	const char* HelpMessage =
		"Help for the chuthulu compiler\n"
		"Arguments:\n"
		"    *.ctu\n"
		"        Any file ending with .ctu will be considered a source file\n"
		"        Only files ending with .ctu will be compiled\n\n"
		"    --verbose or -v\n"
		"        Enable verbose mode\n\n"
		"    --language=<target> or -l=<target>\n"
		"        Chose what the output type should be\n"
		"        'py' for generating python files,\n"
		"        'asm' (default) for generating assembly code or\n"
		"        'jvm' for generating .jar files to be run by the jvm\n\n"
		"    --output=<name> or -o=<name>\n"
		"        Chose the name of the executable file\n"
		"        (spaces are not allowed in the name\n\n"
		"    --debug=<level> or -d=<level>\n"
		"        Chose the level of degub to output\n"
		"        'lexer' for only lexing the file and outputing the lexemes to stdout\n"
		"        'parser' for parsing the file to see if its valid without compiling\n\n"
		;
	printf("%s", HelpMessage);
	exit(1);
}

static void DuplicateArgument(const char* Name)
{
	printf("Duplicate argument: %s\n", Name);
}

static bool EndsWith(const char* Base, const char* Ending)
{
	char* After = strrchr(Base, '.');
	return After && !strcmp(After, Ending);
}

static void ParseArg(char* InArg)
{
	if(!strncmp(InArg, "-h", 2) || !strncmp(InArg, "--help", 6))
		PrintHelp();
	else if(!strncmp(InArg, "-v", 2) || !strncmp(InArg, "--verbose", 9))
	{
		if(Verbose)
			DuplicateArgument("Verbose");
		Verbose = true;
		printf("Verbose mode enabled\n");
	}
	else if(EndsWith(InArg, ".ctu"))
	{
		Append(SourceFiles, InArg);
		if(Verbose)
			printf("Added %s to source file list\n", InArg);
	}
	else if(!strncmp(InArg, "-o=", 3))
	{
		if(NameSet)
			DuplicateArgument("OutputName");
		OutputName = InArg + 3;
		NameSet = true;
		if(Verbose)
			printf("Output name is %s\n", InArg + 3);
	}
	else if(!strncmp(InArg, "--output=", 9))
	{
		if(NameSet)
			DuplicateArgument("OutputName");
		OutputName = InArg + 9;
		NameSet = true;
		if(Verbose)
			printf("Output name is %s\n", InArg + 9);
	}
	else
	{
		printf("Unrecognised argument: %s\n", InArg);
		exit(5);
	}
}

int main(int argc, char** argv)
{
	if(argc < 2)
	{
		printf("Not enough arguments, use --help or -h for help\n");
		return 3;
	}

	for(int Argument = 1; Argument < argc; Argument++)
	{
		ParseArg(argv[Argument]);
	}

	if(NumberOfSourceFiles == 0)
	{
		printf("You need some source files to compile\n");
		exit(5);
	}

	for(int F = 0; F < NumberOfSourceFiles; F++)
	{
		File* CurrentFile = NewFile();
		if(!ReadFile(SourceFiles[F], CurrentFile))
		{
			printf("SourceFile %s does not exist\n", SourceFiles[F]);
			exit(5);
		}
		Lexeme* Lex;
		while((Lex = GetNextLexeme(CurrentFile))->ID != 0)
		{
			DeleteLexeme(Lex);
		}
		printf("Escaped\n");
		Delete(CurrentFile);
	}
}
