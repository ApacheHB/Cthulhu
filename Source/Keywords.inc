KEYWORD(KW_Import, "import")
KEYWORD(KW_Exposing, "exposing")

KEYWORD(KW_DeclareFunction, "def")
KEYWORD(KW_ReturnFunction, "return")

KEYWORD(KW_DeclareConstant, "val")
KEYWORD(KW_DeclareVariable, "var")

KEYWORD(KW_If, "if")
KEYWORD(KW_Else, "else")

KEYWORD(KW_For, "for")
KEYWORD(KW_While, "while")

KEYWORD(KW_Break, "break")
KEYWORD(KW_Skip, "skip") //same as 'continue'

KEYWORD(RK_Scope, "scope")
KEYWORD(RK_Class, "class")
KEYWORD(RK_Self, "self")