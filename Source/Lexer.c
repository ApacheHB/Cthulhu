#include "Lexer.h"

#include "Cthulhu.h"
#include <stdio.h>
#include <stdlib.h>

#define PRINTF(Format, ...) if(Verbose) printf(":%d:%s:" Format "\n", __LINE__, __FILE__, __VA_ARGS__);
#define PRINT(Format) if(Verbose) printf(":%d:%s:" Format "\n", __LINE__, __FILE__);

static bool IsWhitespace(char C)
{
	return C == ' ' || C == '\t' || C == '\n' || C == '\r' || C == '\f' || C == '\v';
}

static bool IsNewline(char C)
{
	return C == '\n' || C == '\r';
}

static Lexeme* LexNumber(File* InFile)
{

}

static Lexeme* LexStringLiteral(File* InFile)
{

}

static Lexeme* LexIdentifier(File* InFile)
{

}

static Lexeme* LexKeyword(File* InFile)
{

}

Lexeme* GetNextLexeme(File* InFile)
{
	char NextChar;
	Lexeme* RetLex = malloc(sizeof(Lexeme));

	//printf("%d\n%c\n", InFile->TrueLocation, InFile->Content[InFile->TrueLocation]);

	NextChar = InFile->Content[InFile->TrueLocation];

	if(NextChar == '\0')
	{
		NextChar = InFile->Content[++InFile->TrueLocation];
		RetLex->ID = 0;
		return RetLex;
	}
	RetLex->ID = 1;

	switch(NextChar)
	{
		case '0' ... '9': {

		}
		case '\n': case '\r': {
			DeleteLexeme(RetLex);
			++InFile->TrueLocation;
			return GetNextLexeme(InFile);
		}
		case '\0': { //return 0 when EOF is reached
			PRINT("EOF");
			RetLex->ID = 0;
			return RetLex;
		}
		default: {
			PRINTF("CurrentLexChar:%c:%d:", InFile->Content[InFile->TrueLocation], InFile->TrueLocation);
			++InFile->TrueLocation;
			RetLex->ID = 1;
			return RetLex;
		}
	}
}

void DeleteLexeme(Lexeme* InLex)
{
	free(InLex);
}